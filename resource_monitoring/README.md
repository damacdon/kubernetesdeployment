# Resource Monitoring

These instructions are partly borrowed from https://github.com/giantswarm/prometheus and https://github.com/danikam/examples/tree/master/scraping-prometheus-k8s-with-metricbeat.

## Assumptions:
 - You have a Kubernetes cluster with three master nodes (node1, node2, and node3). To set up a kubernetes cluster, refer to the instructions in ../terraform_vm_cluster.
 - You would like to run the dedicated monitoring pods on node3. To set up node3 for this, add a custom scheduling taint, and remove the default master node NoSchedule taint:

~~~~
kubectl taint nodes node3 key=value:NoSchedule      # Add the custom scheduling taint
kubectl taint nodes node3 node-role.kubernetes.io/master:NoSchedule-    # Remove the default master node NoSchedule taint
~~~~

If you would like to run the dedicated monitoring pods on a different node, repeat the above for the node you would like to run the monitoring pods on, then change all instances of `node3` in the yaml files to the desired node.

## 1. Local Monitoring with Prometheus + Grafana

Local monitoring of the k8s cluster can be done on a master node with Prometheus and Grafana. The advantages of this local monitoring are that it provides lots of cluster-specific information, including specific jobs running, and it's very quick and easy to bring up. The disadvantages are that the metric data gets deleted quite regularly, and it also seems to become quite flakey as the number of cluster nodes increases (>~10).

Use the setup in the github fork https://github.com/danikam/prometheus of the [k8s prometheus repo](https://github.com/danikam/prometheus) to set up Prometheus monitoring and Grafana visualization on the master node (node 1) with a floating public IP.

~~~~
git clone https://github.com/danikam/prometheus
cd prometheus
kubectl create -f manifests-all.yaml
~~~~

Expose the monitoring pod on port 3000:

~~~~
export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=grafana,component=core" -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward --namespace monitoring $POD_NAME 3000:3000&
~~~~

To view the Grafana visualization:

~~~~
ssh root@[floating IP] -L 3000:127.0.0.1:3000
~~~~

The Grafana page can then be viewed at: http://localhost:3000. The default username and password are both 'admin'.

The Prometheus monitoring setup can be torn down as follows:

~~~~
kubectl delete namespace monitoring
~~~~

## 2. Remote Long-term Resource Metric Storage and Monitoring with Metricbeat + Grafana

Remote cluster monitoring can be done by collecting resource metrics from the cluster nodes with metricbeat, sending them to an elasticsearch instance for long-term storage, and visualizing them with grafana. The advantages are that the metricbeat export of resource metrics to elasticsearch seems to run stably for the cluster sizes tested so far (up to 20 nodes), and the data can remain in the elasticsearch database until the user chooses to delete it. The disadvantages are that there is to k8s-specific info, and there's more overhead required to set up the elasticsearch database if you don't have a pre-existing one. The instructions here assume that you do have an elasticsearch database with a user 'kubernetes' that has permission to write to the database.

### Run metricbeat to export Prometheus resource metrics to the elk elasticsearch instance
#### Authorization
Create a cluster level role binding so that you can manipulate the system level namespace (this is where DaemonSets go)

```
kubectl create clusterrolebinding cluster-admin-binding \
 --clusterrole=cluster-admin --user=<your email address>
```

#### Create the cluster role binding for Metricbeat
```
kubectl create -f metricbeat-clusterrolebinding.yaml
```

#### Check to see if kube-state-metrics is running
```
kubectl get pods --namespace=kube-system | grep kube-state
```
and create it if needed (by default it will not be there)

```
git clone https://github.com/danikam/kube-state-metrics.git kube-state-metrics
kubectl create -f kube-state-metrics/kubernetes
kubectl get pods --namespace=kube-system | grep kube-state
```

### Deploy the Guestbook example

```
kubectl create -f guestbook.yaml
```

### Deploy Metricbeat

#### Run the auto-discover service

If needed, edit the value of the ELASTICSEARCH_USERNAME and ELASTICSEARCH_PASSWORD in metricbeat-prometheus-auto-discover.yaml to reflect the elasticsearch instance that you want to write to, and the username to use. Then, create deploy the auto-discover service to start writing resource metrics to the database.  

```
kubectl create -f metricbeat-prometheus-auto-discover.yaml
```

Check to make sure that the metricbeat-* pods are successfully running on each node in the kube-system namespace:

~~~~
kubectl get pods -n kube-system -o wide
~~~~

### Visualize your data in Grafana

The data written to elasticsearch can be visualized either directly in Kibana, or by exporting it to grafana. We exported it to grafana as a datasource elk-kubernetes-metricbeat, and visualize it here: https://elk2.heprc.uvic.ca/d/TPpCTwWWz/kubernetes-resource-monitoring.
