#!/bin/bash

# Install dependencies
yum install -y emacs git epel-release
yum install -y python3
easy_install pip

# Clone the repo in the root home directory
cd
git clone https://github.com/kubernetes-sigs/kubespray
cd kubespray
git checkout release-2.10

# Install setup requirements
pip install --ignore-installed -r requirements.txt

# Create the inventory directory
cp -rfp inventory/sample inventory/mycluster

# Replace the cluster name as needed
sed -i "s/.*cluster_name: cluster.local.*/cluster_name: cluster.local$1/g" ./inventory/mycluster/group_vars/k8s-cluster/k8s-cluster.yml
sed -i "s/.*cluster_name: cluster.local.*/cluster_name: cluster.local$1/g" ./roles/kubespray-defaults/defaults/main.yaml

# Update the max pods per node as needed (updates to 200 by default)
sed -i "s/.*kubelet_max_pods:.*/kubelet_max_pods: 200/g" /root/kubespray/roles/kubernetes/node/defaults/main.yml
