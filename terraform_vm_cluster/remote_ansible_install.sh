#!/bin/bash

#Make the hosts.ini file with the info for creating the nodes in the cluster
cd /root/kubespray
declare -a IPS=`cat /root/IPs_text`
CONFIG_FILE=inventory/mycluster/hosts.ini python3 /root/inventory.py ${IPS[@]}

# Run the custom ansible playbook to mount the ephemeral storage disk on /var/lib/docker and copy file required for docker registry to all nodes
cd /root/mount_docker_disk
cp ../kubespray/inventory/mycluster/hosts.ini .
export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i hosts.ini --become --become-user=root --user=centos playbook.yml

# Run the kubespray ansible playbook to build the cluster
cd /root/kubespray
if [ "$1" == "scale" ]; then
  echo "------> Scaling Kubernetes cluster"
  ansible-playbook -i inventory/mycluster/hosts.ini --become --become-user=root --user=centos -v scale.yml
else
  echo "------> Building Kubernetes cluster"
  ansible-playbook -i inventory/mycluster/hosts.ini --become --become-user=root --user=centos -v cluster.yml
  cp /usr/local/bin/kubectl /usr/bin/kubectl
fi

# Run the custom ansible playbook to install cvmfs on each node and set up the docker registry
cd /root/install_atlas_reqs
cp ../kubespray/inventory/mycluster/hosts.ini .

# Upgrade to ansible version 2.8
pip install --upgrade ansible==2.8

if [ "$1" == "install" ]; then
    ansible-playbook -i hosts.ini --become --become-user=root --user=centos playbook.yml --extra-vars="install=True"
else
    ansible-playbook -i hosts.ini --become --become-user=root --user=centos playbook.yml --extra-vars="install=False"
fi

# Customize .kube/config file if needed
sed -i "s/.*    user: kubernetes-admin.*/    user: kubernetes-admin$2/g" /root/.kube/config
sed -i "s/.*- name: kubernetes-admin.*/- name: kubernetes-admin$2/g" /root/.kube/config
sed -i "s/.*  name: kubernetes-admin@cluster.local.*/  name: cluster$2/g" /root/.kube/config
sed -i "s/.*current-context: kubernetes-admin@cluster.local.*/current-context: cluster$2/g" /root/.kube/config

# Remove the public rsa key from the root authorized_keys file so it's no longer possible to ssh onto root directly
#grep "^no-port-forwarding" /home/centos/ssh_root_orig > /root/.ssh/authorized_keys
#rm /home/centos/ssh_root_orig
exit
