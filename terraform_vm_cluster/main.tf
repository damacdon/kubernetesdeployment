# Specify public key used to access VM
resource "openstack_compute_keypair_v2" "my-cloud-key" {
  name       = "common-key-tf"
  public_key = "${file("public_key.pub")}"
}

# Add security groups for kubernetes
resource "openstack_compute_secgroup_v2" "k8s-tf-group" {
  name        = "k8s-tf"
  description = "Security group for kubernetes cluster nodes"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 443
    to_port     = 443
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 10250
    to_port     = 10255
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 6443
    to_port     = 6443
    ip_protocol = "tcp"
    cidr        = "188.184.0.0/16"
  }

  rule {
    from_port   = 2379
    to_port     = 2380
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 53
    to_port     = 53
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 53
    to_port     = 53
    ip_protocol = "udp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 6783
    to_port     = 6783
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 30000
    to_port     = 32767
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 9153
    to_port     = 9153
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 3000
    to_port     = 3000
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

}

# Assign a floating IP for the master node
#resource "openstack_networking_floatingip_v2" "floatip_master" {
#  pool = "Public-Network"
#}

# Associate the floating IP to the master node
resource "openstack_compute_floatingip_associate_v2" "floatip_master" {
  #floating_ip = "${openstack_networking_floatingip_v2.floatip_master.address}"
  fixed_ip     = "192.168.245.10"
  floating_ip  = "206.12.91.45"
  instance_id  = "${openstack_compute_instance_v2.master.id}"
}

# Define main master node which will be used for kubespray setup
resource "openstack_compute_instance_v2" "master" {
  name            = "k8master-tf"
  image_name      = "CentOS-7-x64-2019-07"
  flavor_name     = "c8-30gb-186"
  key_pair        = "${openstack_compute_keypair_v2.my-cloud-key.name}"
  security_groups = ["default", "k8s-tf"]
  network {
    name = "${file("openstack_network")}"
    fixed_ip_v4   = "192.168.245.10"
  }

}

# Define additional nodes for the cluster
resource "openstack_compute_instance_v2" "node" {
  name            = "k8node-tf"
  image_name      = "CentOS-7-x64-2019-07"
  flavor_name     = "c8-30gb-186"
  key_pair        = "${openstack_compute_keypair_v2.my-cloud-key.name}"
  security_groups = ["default", "k8s-tf"]
  count		  = 19

  network {
    name = "${file("openstack_network")}"
  }
}
