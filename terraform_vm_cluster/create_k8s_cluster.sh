#!/bin/bash

# Date:     180325
# Purpose:  Create or scale a kubernetes cluster on a set of VMs created by terraform.

# Make sure an argument has been passed, either "install" or "scale"
if [ $# -eq 0 ]; then 
    echo "No arguments supplied. Acceptable arguments are 'install' or 'scale'"
    exit
fi

if [ "$1" != "install" ] && [ "$1" != "scale" ]; then
    echo "'$1' isn't an acceptable argument. Acceptable arguments are 'install' and 'scale'"
    exit
fi

if [ $# -eq 2 ]; then
  echo "Customizing cluster name and context by appending with $2"
  CUSTOMIZE=$2
else
  CUSTOMIZE="1"
fi


# Get the public IP of the master node where kubespray will be set up
MASTER_IP=$(terraform show | grep floating_ip  | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' | awk '!x[$0]++' | tr '\n' ' ' | xargs)

# Use ssh-add to enable passwordless ssh if needed
eval $(ssh-agent -s)
ssh-add private_key

# Temporarily make the root user account accessible via ssh
ssh -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null centos@$MASTER_IP "bash -s" < remote_enable_root_ssh.sh

# Install and setup kubespray on the node if this is an initial setup 
if [ "$1" == "install" ]; then
    ssh -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@$MASTER_IP "bash -s" < remote_kubespray_setup.sh "$CUSTOMIZE"
fi

# Get the IPs of all instances in the cluster, in the format required by kubespray
IPs='('$(terraform show | grep network.0.fixed_ip_v4  | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' | awk '!x[$0]++' | tr '\n' ' ' | xargs)')'

# Add the IPs to a text file
echo $IPs > IPs_text

# Copy the text file with the IPs onto the master node
scp -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null IPs_text root@$MASTER_IP:/root

# Copy the custom inventory.py onto the master node
scp -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null inventory.py root@$MASTER_IP:/root

# Copy the ansible files needed to mount the ephemeral storage disk to /var/lib/docker and install cvmfs onto the master node if this is an initial setup
if [ "$1" == "install" ]; then
    scp -r -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null mount_docker_disk install_atlas_reqs root@$MASTER_IP:/root/
fi

# ssh onto the master node to build kubernetes, forwarding the ssh key password
ssh -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -A root@$MASTER_IP "bash -s" < remote_ansible_install.sh "$1" "$CUSTOMIZE"
