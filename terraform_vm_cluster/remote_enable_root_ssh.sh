#!/bin/bash

# Copy the ssh key for harvester node aipanda177                                                                                
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDcoSE1zJlTDP1dHSPR1ySVqCe7UuTUzlAZBicPc545tjkNudDa51TrwWCT/wts0NueF/jhgQxmWTO2X0InW4IGus0HWk23ic7z5oyEn8GA+QP0OacSMhW8Dq7oVfvZzaqM2KXrQ2oXC8AiYZeqc9jqJnbgbAD36xVpwAcyZMsf3fJg0JsM7dTFQYVj/q8R0qmb6+4AAiHH0RTePlJo3u5GQfTrp0sZI2FwmU+n9iPkm7jUl+CWe1Pt4UCNP449ADR0cc58UNAQmQN2UvqG3TAhideJ7wUUaIisPrAtG5rWYuhWRUQHXkSVIhS41w7KbJAdh8rWR+ztQmsNRODuT4D9 atlpan@aipanda177.cern.ch >> /home/centos/.ssh/authorized_keys

# Copy the ssh key for the arbutus VM that regularly check the ssh connection
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDsSnzFLEXvw2BWTi4BbS952mgZaYhZxjvp9vY5xUNbpp/UF6KDDcxXpEl1HY/lkfD90ilqapavjJeovmFAJ5Nvle3UwCwrQuMTQS9UD5AU90AOtHGcxA3trIJPPCFAlYUTHQ6aH3qjrWvvpsKgAXoppfhKdxhQf2BwJwJ5s/kpBQtXgnDXYDkTIN/komQiZm5Fhs3U2AAMSyjTAkN7kXd1eSxu6VoF3n2oJYsOM9lEz0nf/eNU2VNgWvLlgi9jQXIC0puDlGd5RT3Jt6nmkBFsvUDFfu0YK5eesodjFvBd6t67OeloPeWnb58Ol6WLa9iyNS5P6sT/W8C6Ai39wWMP root@check-ssh-master.novalocal >> /home/centos/.ssh/authorized_keys

# Create a temporary copy of the authorized_keys file
cp /home/centos/.ssh/authorized_keys ssh_tmp

# Switch to root user for copying stuff owned by root
sudo su

echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDcoSE1zJlTDP1dHSPR1ySVqCe7UuTUzlAZBicPc545tjkNudDa51TrwWCT/wts0NueF/jhgQxmWTO2X0InW4IGus0HWk23ic7z5oyEn8GA+QP0OacSMhW8Dq7oVfvZzaqM2KXrQ2oXC8AiYZeqc9jqJnbgbAD36xVpwAcyZMsf3fJg0JsM7dTFQYVj/q8R0qmb6+4AAiHH0RTePlJo3u5GQfTrp0sZI2FwmU+n9iPkm7jUl+CWe1Pt4UCNP449ADR0cc58UNAQmQN2UvqG3TAhideJ7wUUaIisPrAtG5rWYuhWRUQHXkSVIhS41w7KbJAdh8rWR+ztQmsNRODuT4D9 atlpan@aipanda177.cern.ch >> /root/.ssh/authorized_keys

# Copy public rsa key into the root authorized_keys file so we can temporarily ssh onto root directly
cp /root/.ssh/authorized_keys ssh_root_orig
cp ssh_tmp /root/.ssh/authorized_keys
rm ssh_tmp
