# VM Kubernetes Cluster with Terraform and Kubespray

The terraform (.tf) files and shell scripts in this directory allows one to create an arbitrary number of master and worker VMs on an openstack cloud using terraform, and build a kubernetes cluster with the openstack VMs as nodes using Kubespray. Note that the openstack cloud must have at least one floating public IP address available.

# Contact Information

In case of questions or bug reports, don't hesitate to contact Danika MacDonell <danikam1@uvic.ca> or Rolf Seuster <seuster@uvic.ca>.

# Instructions for Use

## 1. Download and source Openstack RC file

On your browser, navigate to the API Access page for the openstack cloud on which you plan to setup the VM cluster. On the Arbutus cloud, for example, this page would be: https://arbutus.cloud.computecanada.ca/project/api_access/

Download the openstack RC file by clicking the "Download OpenStack RC File" tab on the upper right, and selecting "OpenStack RC File (Identity API v3)". Move the RC file to this directory (kubernetesdeployment/terraform_vm_cluster) and source it.

Eg. From this directory:

~~~~
mv ~/Downloads/rpp-rsobie-openrc.sh .
source rpp-rsobie-openrc.sh
~~~~

You will be asked to type in your password for the openstack project.

## 2. Initial Setup

If needed, change the public rsa key to be used for creating, copying to, and ssh-ing onto the VMs in the public_key.pub file (it's currently set to the heprc common key). 

If needed change the name of the network that the VMs will use in openstack_network (it's currently set to rpp-rsobie-network).

Make a soft link named *private_key* to the private key corresponding to the public key in public_key.pub:

Eg.
~~~~
ln -s ~/.ssh/heprc_common_key private_key
~~~~

Lastly, initialize terraform:

~~~~
terraform init
~~~~

## 3. Create the VMs on openstack

Change the *count* line in the *resource "openstack_compute_instance_v2" "nodes"* section in main.tf to set the number of additional VM nodes you would like in addition to the main master node. Note that kubespray may assign some of these additional nodes as masters as well as the main master node. You can also change the flavor names as needed using the flavor_name parameter. 

Add additional security rules under *resource "openstack_compute_secgroup_v2" "k8s-tf-group"* if needed.

Update the private and floating public IPs of the master node under *resource "openstack_compute_floatingip_associate_v2" "floatip_master"* in main.tf such that the floating IP is one of the floating IPs available to your project, and the private IP is consistent with the range of private IPs allocated to the project. 

**If there's already another cluster on the cloud that was created with these instructions:**
 * The keypair will already exist, so comment out the entire `resource "openstack_compute_keypair_v2" "my-cloud-key" {` section.
 * The security group will already be set up, so comment out the entire `resource "openstack_compute_secgroup_v2" "k8s-tf-group" {` section.
 * Change the `key_pair` in the `resource "openstack_compute_instance_v2" "master" {` and `resource "openstack_compute_instance_v2" "node" {` sections from `"${openstack_compute_keypair_v2.my-cloud-key.name}"` to `"common-key-tf"`

Once any needed edits have been made to the main.tf and openstack_network files, the execution plan can be checked using:

~~~~
terraform plan
~~~~

If you are satisfied with the execution plan, execute it as follows to create the VMs on openstack:

~~~~
terraform apply
~~~~

## 4. Build the Kubernetes cluster

Run the script create_k8s_cluster.sh with the 'install' option to build a kubernetes cluster on the openstack VMs using kubespray. The script will initially ask for your ssh key password (if applicable). 

Note that if an ssh key password needed, it will be forwarded to k8master-tf via ssh-client, so a reasonable network connection is needed to ensure that the kubespray setup won't time out waiting for info from the ssh-client.

~~~~
./create_k8s_cluster.sh install
~~~~

Once this is finished, you can ssh onto the k8master-tf node and check that the cluster is up and healthy:

For example, if the floating IP is 206.12.90.89:

~~~~
ssh -i private_key -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null centos@206.12.90.89
sudo su
kubectl get nodes
~~~~

You should see something like:

~~~~
NAME    STATUS   ROLES         AGE   VERSION
node1   Ready    master,node   71m   v1.12.6
node2   Ready    master,node   38m   v1.12.6
node3   Ready    node          38m   v1.12.6
~~~~

## 7. Scaling up the VM cluster

Additional nodes can be added to the VM cluster by increasing the *count* variable under *resource "openstack_compute_instance_v2" "node"* in the main.tf file. Apply this update by re-running 'terraform apply':

Eg.
~~~~
source rpp-rsobie-openrc.sh         # Source the OpenRC file if needed
terraform init                      # Initialize terraform if needed
terraform apply
~~~~

Run create_k8s_cluster.sh with the 'scale' option to add these additional nodes the to the k8s cluster:

~~~~
./create_k8s_cluster.sh scale
~~~~

## 8. Destroying the terraform VMs

If at any point you need to tear down the openstack VMs and start from scratch, just run:

~~~~
source rpp-rsobie-openrc.sh         # This should be the openstack RC file you downloaded from openstack
terraform init
terraform destroy
~~~~

Currently, the 'terraform destroy' command may need to be run twice in order to fully remove the access key and security group.