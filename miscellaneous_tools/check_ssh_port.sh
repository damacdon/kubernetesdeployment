#!/bin/bash
# Date:    190607
# Author:  Danika MacDonell
# Purpose: Check whether it's possible to ssh onto the main master node of the arbutus cluster, where the private IPs of the master nodes are set as variables in the script.

# Usage: This is probably best used as a cron job. In this case, a public rsa key should be generated using ssh-keygen for the machine that this is running on if there isn't already one. The public ssh key should then be copied to the authorized_keys file for the centos user on the master node (this is currently done by default in the terraform+kubespray setup scripts). 
# I currently have it in the crontab file as follows:
#  */30 * * * * /root/kubernetesdeployment/miscellaneous_tools/check_ssh_port.sh  

MASTER_NODE=192.168.245.10
#MASTER_NODE=192.168.245.34   # Hostname of this machine (can temporarily disable sshd for testing)
EMAIL_ADDR=danika.m16@gmail.com

# Run the actual ssh port forwarding in the background. Exit on failure so that we don't create a new background ssh process every time the script is run.
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l centos $MASTER_NODE -N -f -o "ExitOnForwardFailure yes" -o "ConnectTimeout 10" 
RETURN_VAL=$?

# If the ssh attempt failed, send an email indicating that this happened, and log the event locally
if [ $RETURN_VAL -ne 0 ]; then  
  SUBJECT="Subject: ssh Attempt Failed on k8s Master Node"
  CONTENT="Warning: an attempt to ssh onto the kubernetes master node $MASTER_NODE FAILED at the following time: $(date)"
  echo -e "${SUBJECT}\n\n${CONTENT}\n" | /sbin/sendmail $EMAIL_ADDR
  echo -e "$(date -u):\t attempt to ssh onto the kubernetes master node $MASTER_NODE FAILED" >> /var/log/k8s_check_ssh_port.log
else
  # Otherwise, locally log the fact that the ssh attempt succeeded
  echo -e "$(date -u):\t attempt to ssh onto the kubernetes master node $MASTER_NODE SUCCEEDED" >> /var/log/k8s_check_ssh_port.log
fi

