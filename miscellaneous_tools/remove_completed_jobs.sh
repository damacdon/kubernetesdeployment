# List of config files and cluster names for all member clusters (assumed to be located in ~/.kube directory)
configs=(config config2)
contexts=(cluster1 cluster2)

# Create the KUBECONFIG string
config_str=~/.kube/${configs[0]}
for CONFIG in "${configs[@]:1}"
do
  config_str=${config_str}:~/.kube/${CONFIG}
done
export KUBECONFIG=${config_str}

# Remove all completed pods on all clusters
for CONTEXT in "${contexts[@]}"
do
    kubectl get pods --context $CONTEXT | grep Completed | awk '{print $1}' | xargs kubectl delete pod --context $CONTEXT
done

