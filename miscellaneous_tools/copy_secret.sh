# Bash script to copy the proxy-secret from the primary k8s cluster to a secondary cluster, whose config is located in .kube/config2 (see instructions for setting up the multicluster scheduler in ../multicluster_scheduler).

configs=(config config2)
contexts=(cluster1 cluster2)

# Create the KUBECONFIG string
config_str=~/.kube/${configs[0]}
for CONFIG in "${configs[@]:1}"
do
  config_str=${config_str}:~/.kube/${CONFIG}
done
export KUBECONFIG=${config_str}

# Distribute latest proxy-secret to member clusters
kubectl get secret proxy-secret -o yaml > proxy-secret.yaml
for CONTEXT in "${contexts[@]:1}"
do
  kubectl delete secret proxy-secret --context $CONTEXT
  kubectl apply -f proxy-secret.yaml --context $CONTEXT
done
rm -f proxy-secret.yaml
