# Miscellaneous Tools

This directory contains miscellaneous tools used for the harvester integration, with the context and usage instructions for each tool included in this README. 

## Cron job to delete containers stuck in ContainerCreating state

We noticed on the baremetal cluster that occasionally, a new pod will get stuck in the ContainerCreating stage, and if this pod is manually deleted, the pod launched by k8s to replace it will generally run without this issue. We still don't understand the underlying reason for this occasional pod-launching issue. For now, an effective fix is to run the shell script delete_ContainerCreating_pods as a cron job on one of the master nodes every 20 minutes to delete any containers that have been in the ContainerCreating state for at least 10 minutes. The instructions for setting tihs up are as follows:

### 1. Copy the shell script delete_ContainerCreating_pods to /usr/local/bin

~~~~
cp delete_ContainerCreating_pods /usr/local/bin
~~~~

### 2. Create the directory /root/delete_ContainerCreating_pods_output to contain:
  * a daily log [date].log indicating any action(s) taken each time the cron job is run
  * a log file kubectl_output_[day]_[hour].log with the output of 'kubectl get pods -o wide' showing the state of the all pods immediately before delete_ContainerCreating_pods is run. These logs are overwritten every week.
  * a log file kubectl_describe_[pod name]_[date].log showing the output of 'kubectl describe pod [pod_name]' showing the reason why pod [pod_name] is stuck in the ContainerCreating state immediately before it's deleted.
 
~~~~
mkdir /root/delete_ContainerCreating_pods_output
~~~~

### 3. Open the crontab, and add a line to run the above shell script every 20 minutes

~~~~
crontab -e
# Add the following line: '*/20 * * * * /usr/local/bin/delete_ContainerCreating_pods'
~~~~

## Cron job to check the ssh port on the main master node

Occasionally, we've noticed that the ssh service on one or both of the master nodes will crash. Since we currently use an ssh tunnel to connect the Harvester node with one of the master nodes, an ssh service crash will prevent jobs from being submitted to the cluster rom Harvester. Because of this, it's important to be notified ASAP if an ssh crash does occur on the master node that Harvester submits jobs to. 

The shell script check_ssh_port.sh was designed to be run as a cron job from a remote machine with consistent network access. Instructions for use are listed below.

### 1. Launch and/or log in to a stable remote machine to be used for the ssh monitoring. I launched a 1-core VM on the arbutus cluster for this. 

### 2. If needed, download the sendmail client onto the ssh monitoring machine

~~~~
sudo yum -y install sendmail sendmail-cf
~~~~

### 3. If needed, generate a public ssh key, and copy it into the authorized_keys file for the centos user on the master node to be monitored (assuming the master node is a CentOS machine). Don't set a password for the ssh key. 

~~~~
su centos       # Make sure you're centos user
ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub
# Copy the public ssh key that gets printed onto the terminal
~~~~

~~~~
# ssh onto the master node to be monitored
su centos       # Make sure you're centos user
vi ~/.ssh/authorized_keys       # Open up the authorized_keys file
# Paste the public ssh key into the authorized_keys file on a new line
~~~~

### 4. If needed, update the variables MASTER_NODE and EMAIL_ADDR. Then edit the crontab file:

~~~~
crontab -e
~~~~

and add the following line to check the ssh port every 30 minutes:

~~~~
*/30 * * * * /path/to/check_ssh_port.sh
~~~~
