#!/anaconda3/bin/python

"""
  Date:     190227
  Purpose:  Select variables of interest from the elasticsearch database on elk contianing info about computing jobs on the PANDA server, and compare job metrics for different modification hosts and job types.
  
            Adapted from code on gitlab (https://gitlab.cern.ch/seuster/ES-cloud-jobmonitoring/tree/master/ATLAS) written by Rolf Seuster.
"""

from elasticsearch import Elasticsearch
from elasticsearch.connection import create_ssl_context
import numpy as np
import re
import matplotlib.pyplot as plt
plt.rc('xtick', labelsize=20)
plt.rc('ytick', labelsize=20)

############################## Functions Defined Here ##############################

# Function to separate metrics according to the job type, and return the separated metrics along with their stats
def Separate_JobID(metric, jobtypeid, destinationdblock):
  list=[]
  jobids=[]
  dblocks=[]
  nevents=[]
  means=[]
  stdevs=[]
  mins=[]
  maxes=[]
  
  for id in np.unique(jobtypeid):
    metric_jobID = metric[jobtypeid==id]
    list.append(metric_jobID)
    indices = find_indices(jobtypeid, lambda x: x==id)
    dblocks.append(destinationdblock[indices[0]])
    jobids.append(jobtypeid[indices[0]])
    
    nevents.append(len(metric_jobID))
    means.append(np.mean(metric_jobID))
    stdevs.append(np.std(metric_jobID))
    mins.append(np.min(metric_jobID))
    maxes.append(np.max(metric_jobID))
  
  return list, dblocks, np.asarray(jobids), np.asarray(nevents), np.asarray(means), np.asarray(stdevs), np.asarray(mins), np.asarray(maxes)

# Function to find the indices of an array satisfying the given condition
def find_indices(lst, condition):
  return [i for i, elem in enumerate(lst) if condition(elem)]

####################################################################################

# Create an ssl context from a cert cacert.pem
# To create a self-signed cert, type the following into the terminal (make sure the openssl package is downloaded first):
#   >> openssl req -newkey rsa:2048 -nodes -x509 -days 365 -out cacert.pem
context = create_ssl_context(cafile="cacert.pem")

# Connect to the existing elasticsearch database on elk.heprc.uvic.ca
es = Elasticsearch(['https://castor:OhCanadensisMightyAnimal@elk.heprc.uvic.ca:19200'],
                   ssl_context=context)

# Define some data selection cuts: currently, how recent the data has to be and the jobstatus
# The selection "computingsite.keyword": "ANALY_IAAS" specifies that the job ran on the k8s cluster
selection={
    "query": {
      "bool": {
        "must": [
                 {"range": {"posted": {"gt": "now-100h"}}},
                 {"term": {"jobstatus": "finished"}},
                 {"term": {"computingsite.keyword": "ANALY_IAAS"}}],
        "must_not": [
                       {"term": {"jobstatus": "starting"}},
                       {"term": {"jobstatus": "cancelled"}},
                       {"term": {"jobstatus": "closed"}},
                       {"term": {"jobstatus": "failed"}}]}}}

# Specify the modification hosts we want to consider, and what the modificationhostid will start with for each
# Must be the first bit of the job name before '-[numbers]'
#mod_hosts = ["k8master", "docker-test"]
#mod_hosts_startswith = ["atlasadc-job-bm", "atlasadc-job-dt"]

mod_hosts = ["k8master_bm", "k8master_vm"]
mod_hosts_startswith = ["atlasadc-job-bm", "atlasadc-job-bm-vm"]

#mod_hosts = ["k8master_vm"]
#mod_hosts_startswith = ["atlasadc-job-bm-vm"]

nHosts = len(mod_hosts)

# Collect the data of interest from the elasticsearch database.
# "test2" indicates which PANDA queues we're selecting from
res = es.search(index="test2",
                doc_type="test2",
                body=selection,
                size=10000)

# Number of bins for histograms
Nbins=20

############### Initialize lists to contain the job info of interest ###############

# Time
durationsec=[]
maxwalltime=[]

# CPU
cpuconsumptiontime=[]
maxcpucount=[]

# Memory/storage
avgpss=[]
minramcount=[]
maxpss=[]
diskio=[]
maxdiskcount=[]
avgswap=[]
maxswap=[]


# Reading and writing
raterbytes=[]
raterchar=[]
ratewbytes=[]
ratewchar=[]
totrbytes=[]
totrchar=[]
totwbytes=[]
totwchar=[]

# Miscellaneous
modificationhost=[]   # Of interest since we want to know which computer the jobs ran on
modhostid=[]          # Numerical ID obtained from the modificationhost string
failedattempt=[]
creationhost=[]
destinationdblock=[]
jobtypeid=[]    # Defined by me to give a numerical ID to different job types
####################################################################################


######### Go through the jobs and fill the lists with the info of interest #########

for entry in res['hits']['hits']:
  if("install.validate" not in entry['_source']['destinationdblock']):
    
    # Get the modification host ID from the modificationhost string
    collectData=False
    for iHost in np.arange(nHosts):
      if (entry['_source']['modificationhost']).startswith(mod_hosts_startswith[iHost]) and (entry['_source']['modificationhost'])[len(mod_hosts_startswith[iHost])+1].isdigit():
        modhostid.append(iHost)
        collectData=True
  
    if collectData:
      # Time
      durationsec.append(entry['_source']['durationsec'])
      maxwalltime.append(entry['_source']['maxwalltime'])
      
      # CPU
      cpuconsumptiontime.append(entry['_source']['cpuconsumptiontime'])
      maxcpucount.append(entry['_source']['maxcpucount'])
      
      # Memory/storage
      avgpss.append(entry['_source']['avgpss'])
      minramcount.append(entry['_source']['minramcount'])
      maxpss.append(entry['_source']['maxpss'])
      diskio.append(entry['_source']['diskio'])
      maxdiskcount.append(entry['_source']['maxdiskcount'])
      avgswap.append(entry['_source']['avgswap'])
      maxswap.append(entry['_source']['maxswap'])

      # Reading and writing
      raterbytes.append(entry['_source']['raterbytes'])
      raterchar.append(entry['_source']['raterchar'])
      ratewbytes.append(entry['_source']['ratewbytes'])
      ratewchar.append(entry['_source']['ratewchar'])
      totrbytes.append(entry['_source']['totrbytes'])
      totrchar.append(entry['_source']['totrchar'])
      totwbytes.append(entry['_source']['totwbytes'])
      totwchar.append(entry['_source']['totwchar'])

      # Miscellaneous
      modificationhost.append(entry['_source']['modificationhost'])
      failedattempt.append(entry['_source']['failedattempt'])
      creationhost.append(entry['_source']['creationhost'])
      destinationdblock.append(entry['_source']['destinationdblock'])

      # Parse a job type ID from the destinationdblock
      jobtypeid.append(int(re.findall(r'\d+', entry['_source']['destinationdblock'])[1]))


####################################################################################


################### Convert the numerical lists to numpy arrays ####################

# Time
durationsec = np.asarray(durationsec)
maxwalltime = np.asarray(maxwalltime)

# CPU
cpuconsumptiontime = np.asarray(cpuconsumptiontime)
maxcpucount = np.asarray(maxcpucount)

# Memory/storage
avgpss = np.asarray(avgpss)
minramcount = np.asarray(minramcount)
maxpss = np.asarray(maxpss)
diskio = np.asarray(diskio)
maxdiskcount = np.asarray(maxdiskcount)
avgswap = np.asarray(avgswap)
maxswap = np.asarray(maxswap)

# Reading and writing
raterbytes = np.asarray(raterbytes)
raterchar = np.asarray(raterchar)
ratewbytes = np.asarray(ratewbytes)
ratewchar = np.asarray(ratewchar)
totrbytes = np.asarray(totrbytes)
totrchar = np.asarray(totrchar)
totwbytes = np.asarray(totwbytes)
totwchar = np.asarray(totwchar)

# Miscellaneous
failedattempt = np.asarray(failedattempt)
jobtypeid = np.asarray(jobtypeid)
modhostid = np.asarray(modhostid)

####################################################################################


####################### Calculate stats and plot histograms ########################

# Set the variables you want to plot
metrics_2_plot=[cpuconsumptiontime, avgpss/1e6, maxpss/1e6, raterbytes, ratewbytes, totrbytes, totwbytes]
metrics_str=["cpuconsumptiontime", "avgpss", "maxpss", "raterbytes", "ratewbytes", "totrbytes", "totwbytes"]
metrics_title=["CPU Consumption Time [cpuconsumptiontime] (s)", "Average Proportional Set Size [avgpss] (GB)", "Maximum Proportional Set Size [maxpss] (GB)", "Read Rate [raterbytes] (bytes/s)", "Write Rate [ratewbytes] (bytes/s)", "Total Bytes Read [totrbytes] (bytes)", "Total Bytes Written [totwbytes] (bytes)"]
N_metrics = len(metrics_2_plot)

# Plot histograms and calculate statistics for all job types
for iMetric in np.arange(N_metrics):
  
  # Calculate stats for all job types
  nevents_all = np.zeros(nHosts)
  means_all = np.zeros(nHosts)
  stdevs_all = np.zeros(nHosts)
  mins_all = np.zeros(nHosts)
  maxes_all = np.zeros(nHosts)

  for iHost in np.arange(nHosts):
    metric_host = (metrics_2_plot[iMetric])[modhostid==iHost]
    nevents_all[iHost] = len(metric_host)
    means_all[iHost] = np.mean(metric_host)
    stdevs_all[iHost] = np.std(metric_host)
    mins_all[iHost] = np.min(metric_host)
    maxes_all[iHost] = np.max(metric_host)

  # Histogram for all job types
  fig = plt.figure(figsize =(14, 9));ax=fig.add_subplot(1,1,1)

  # Determine absolute max and min for creating bins
  abs_min=min(mins_all)
  abs_max=max(maxes_all)

  # Create bins
  bin_centers=np.linspace(abs_min, abs_max, Nbins)
  bin_width=bin_centers[1]-bin_centers[0]
  bin_edges= np.append(bin_centers-bin_width/2, abs_max+bin_width/2)

  ax.hist((metrics_2_plot[iMetric])[modhostid==0], bins=bin_edges, label="Host: %s\nEvents: %d\nMean: %f\nstd: %f"%(mod_hosts[0], nevents_all[0], means_all[0], stdevs_all[0]), alpha=0.7)

  for iHost in np.arange(1, nHosts):
    ax.hist((metrics_2_plot[iMetric])[modhostid==iHost], bins=bin_edges, label="Host: %s\nEvents: %d\nMean: %f\nstd: %f"%(mod_hosts[iHost], nevents_all[iHost], means_all[iHost], stdevs_all[iHost]), alpha=0.7)

  ax.set_title("All Job Types", fontsize=22)
  ax.set_xlabel(metrics_title[iMetric], fontsize=20)
  ax.set_ylabel("Frequency", fontsize=20)
  plt.tight_layout()
  ax.legend(prop={'size': 20}, ncol=2)
  plt.savefig("Plots/%s_hist.png"%metrics_str[iMetric])
  plt.close()

  # Separate the data according to the job type ID
  lists_host=[]
  dblocks_host=[]
  jobids_host=[]
  nevents_host=[]
  means_host=[]
  stdevs_host=[]
  mins_host=[]
  maxes_host=[]

  for iHost in np.arange(nHosts):
    host_indices = find_indices(modhostid, lambda x: x==iHost)
    list, dblocks, jobids, nevents, means, stdevs, mins, maxes = Separate_JobID((metrics_2_plot[iMetric])[modhostid==iHost], jobtypeid[modhostid==iHost], [destinationdblock[i] for i in host_indices])
    lists_host.append(list)
    dblocks_host.append(dblocks)
    jobids_host.append(jobids)
    nevents_host.append(nevents)
    means_host.append(means)
    stdevs_host.append(stdevs)
    mins_host.append(mins)
    maxes_host.append(maxes)

    # Save the stats to a file
    file = open("Tables/%s_%s_stats.txt"%(mod_hosts[iHost], metrics_str[iMetric]), "w")
    file.write("%6s%12s%18s%18s%18s%18s\n"%("Job ID", "Events", "Mean", "Std", "Min", "Max"))
    for iID in np.arange(len(list)):
      file.write("%6s%12s%18.4e%18.4e%18.4e%18.4e\n"%(jobids[iID], nevents[iID], means[iID], stdevs[iID], mins[iID], maxes[iID]))
    
    file.write("%6s%12s%18.4e%18.4e%18.4e%18.4e\n"%("All", nevents_all[iHost], means_all[iHost], stdevs_all[iHost], mins_all[iHost], maxes_all[iHost]))
    file.close()


  # Stacked histogram according to job type ID
  for iHost in np.arange(nHosts):
    fig = plt.figure(figsize =(14, 9));ax=fig.add_subplot(1,1,1)
    ax.set_xlabel(metrics_title[iMetric], fontsize=20)
    ax.set_ylabel("Frequency", fontsize=20)
    ax.set_title("Host: %s"%mod_hosts[iHost], fontsize=22)
    plt.tight_layout()
    ax.hist(lists_host[iHost], stacked=True, label=dblocks_host[iHost])
    ax.legend(prop={'size': 18})
    plt.savefig("Plots/%s_%s_hist_stacked.png"%(mod_hosts[iHost], metrics_str[iMetric]))
    plt.close()

  # Histogram for each job type ID

  # Get all unique job IDs for all hosts
  jobids_all = np.zeros(0)
  for iHost in np.arange(nHosts):
    jobids_all = np.append(jobids_all, jobids_host[iHost])
  jobids_all = np.unique(jobids_all)


  # Individual histograms according to job type ID
  for jobid in jobids_all:
    
    # Determine if at least two hosts have at least two events to plot as a histogram for the given job ID
    nPlottable=0    # Number of plottable hosts (i.e. at least two events to plot)
    abs_min=9e20    # Absolute minimum over all hosts (for creating bins)
    abs_max=-9e20
    
    i_LastHost=0    # Last host that included the job ID under consideration. Needed for job ID labels in the plot
    for iHost in np.arange(nHosts):
      if jobid in jobids_host[iHost]:
        idx_jobid = find_indices(jobids_host[iHost], lambda x: x==jobid)[0]
        if len((lists_host[iHost])[idx_jobid])>1:
          nPlottable+=1
          i_LastHost=iHost
          
          # Determine absolute max and min
          if min((lists_host[iHost])[idx_jobid]) < abs_min: abs_min=min((lists_host[iHost])[idx_jobid])
          if max((lists_host[iHost])[idx_jobid]) > abs_max: abs_max=max((lists_host[iHost])[idx_jobid])

    # Plot histograms for all hosts together if there are at least two hosts to plot
    if nPlottable >= 1:
      
      # Create bins
      bin_centers=np.linspace(abs_min, abs_max, Nbins)
      bin_width=bin_centers[1]-bin_centers[0]
      bin_edges=np.append(bin_centers-bin_width/2, abs_max+bin_width/2)

      fig = plt.figure(figsize =(14, 9));ax=fig.add_subplot(1,1,1)
      ax.set_xlabel(metrics_title[iMetric], fontsize=20)
      ax.set_ylabel("Frequency", fontsize=20)
      ax.set_title("TID: %d"%(jobids_host[i_LastHost][idx_jobid]), fontsize=20)
      plt.tight_layout()
      for iHost in np.arange(nHosts):

        bin_centers=np.linspace(abs_min, abs_max, Nbins)
        bin_width=bin_centers[1]-bin_centers[0]
        bin_edges= np.append(bin_centers-bin_width/2, abs_max+bin_width/2)
    
        if jobid in jobids_host[iHost]:
          idx_jobid = find_indices(jobids_host[iHost], lambda x: x==jobid)[0]
          plotted=0
          if len((lists_host[iHost])[idx_jobid])>1:
            ax.hist((lists_host[iHost])[idx_jobid], bins=bin_edges, label="%s\nEvents: %d\nMean: %f\nstd: %f"%(mod_hosts[iHost], nevents_host[iHost][idx_jobid], means_host[iHost][idx_jobid], stdevs_host[iHost][idx_jobid]), alpha=0.7)
      ax.legend(prop={'size': 20}, ncol=2)
      plt.savefig("Plots/%s_hist_jobID%d.png"%(metrics_str[iMetric], jobids_host[i_LastHost][idx_jobid]))
      plt.close()

####################################################################################
