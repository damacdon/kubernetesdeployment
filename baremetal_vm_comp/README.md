# Baremetal/VM Comparison

The code in this directory collects resource monitoring metrics from the elasticsearch database on elk.heprc.uvic.ca for the jobs that ran on the PANDA queue ANALY_IAAS, which uses the Kubernetes cluster at UVic. The code also compares various performance metrics for different worker nodes (i.e. "hosts").

