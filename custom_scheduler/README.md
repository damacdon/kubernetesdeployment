# Code and instructions for custom load packing kubernetes scheduler

A custom load packing scheduler was created to try and optimize the packing of jobs into k8s cluster nodes. It's based on instructions listed in the [instructions for running harvester with a k8s cluster](https://github.com/PanDAWMS/panda-harvester/wiki/Specific-instructions-to-run-with-Kubernetes-cluster#k8s-scheduling-packing-nodes-rather-than-spreading). These instructions were written assuming that the kube-scheduler runs as a systemctl service directly on a master node. However, because more recent versions of k8s run the scheduler in a dedicated pod, and recommend configuring multiple schedulers in addition to the default scheduler if custom scheduling is desired, some updates were needed, which are listed here. The instructions largely follow the example provided by k8s [here](https://kubernetes.io/docs/tasks/administer-cluster/configure-multiple-schedulers/), with some minor tweaks. 

## 0. If needed, adjust taints on node1 to allow the custom scheduling pod to run on this node

By default, the custom scheduling pod will attempt to run on node1, since it is already dedicated to monitoring (see the README in ../resource_monitoring). If the taints on node1 haven't already been adjusted for resource monitoring, adjust them as follows:

~~~~
kubectl taint nodes node1 key=value:NoSchedule      # Add the custom scheduling taint
kubectl taint nodes node1 node-role.kubernetes.io/master:NoSchedule-    # Remove the default master node NoSchedule taint
~~~~

If you would like to run the custom scheduling pod on a different node, repeat the above for the node you would like to run the monitoring pods on, then change `node1` in the file node-packing-scheduler.yaml to the desired node.


## 1. Clone and build the k8s code from github if needed

If you're using a k8s version other than 1.14.x, you'll need to make the kube-scheduler binary for your k8s version. You can check your k8s version as follows:

~~~~
kubectl version
~~~~

I get the following output, indicating that my k8s version is 1.14.3:

~~~~
Client Version: version.Info{Major:"1", Minor:"14", GitVersion:"v1.14.3", [...]
Server Version: version.Info{Major:"1", Minor:"14", GitVersion:"v1.14.3", [...]
~~~~

If your version is something other than 1.14.x, please follow the instructions below to make your own kube-scheduler binary.

In order to build the k8s code, it's necessary to first install the latest version of go ang gcc:

~~~~
# Install gcc
yum -y install gcc

# Install go
wget https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.12.7.linux-amd64.tar.gz

# Add /usr/local/go/bin to the PATH environment variable
export PATH=$PATH:/usr/local/go/bin
~~~~

To clone and build the k8s code, run:

~~~~
git clone https://github.com/kubernetes/kubernetes.git
cd kubernetes

# If needed, check out the version of k8s that you're using
git checkout release-[your version (eg. 1.13)]
make
~~~~

Copy the kube-scheduler binary to the main repo level, then you can remove the (rather large...) kubernetes directory:

~~~~
cd ..
cp kubernetes/_output/local/bin/linux/amd64/kube-scheduler .
rm -rf kubernetes
~~~~

## 2. Build the container image and push it to docker hub

Login to docker, build the kube-scheduler container image, and push the image to docker hub.

~~~~
docker login -u [your-docker-username]
docker build -t [your-docker-username]/node-packing-scheduler .       # Name it whatever you want, just be sure to include your docker username at the beginning
docker push [your-docker-username]/node-packing-scheduler
~~~~

Update the image name (line 51) in node-packing-scheduler.yaml with the name of the image you pushed to docker hub.

## 3. Run the second scheduler in the cluster

~~~~
kubectl create -f node-packing-scheduler.yaml
~~~~

Check that the scheduler pod is running:

~~~~
kubectl get pods --namespace=kube-system
~~~~

You should see something like:

~~~~
NAME                                                 READY   STATUS              RESTARTS   AGE
node-packing-scheduler-7df5697487-55n27              0/1     Running             0          5s
~~~~

Edit the `system: kube-scheduler` cluster role:

~~~~
kubectl edit clusterrole system:kube-scheduler
~~~~

Add `- node-packing-scheduler` under `kube-scheduler` in `resourceNames`, and copy the following to the bottom of the file:

~~~~
- apiGroups:
  - storage.k8s.io
  resources:
  - storageclasses
  verbs:
  - watch
  - list
  - get
~~~~

The scheduler can now be tested by creating a pod that gets scheduled by the custom scheduler by creating the scheduler_test_pod.yaml file (see line 8 in scheduler_test_pod.yaml for an example of how to specify that pods should use the custom scheduler):

~~~~
kubectl create -f scheduler_test_pod.yaml
~~~~

After 30s or so, you should see it scheduled and running:

~~~~
kubectl get pods
~~~~

should include something like:

~~~~
NAME                               READY   STATUS      RESTARTS   AGE
...
annotation-second-scheduler        1/1     Running     0          30s
...
~~~~