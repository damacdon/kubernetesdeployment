# KubernetesDeployment

This repo contains code used for R&D towards developing a kubernetes (k8s) cluster at UVic. The goal is to develop a k8s cluster that can act as a computing site to run ATLAS production jobs delivered to the cluster from a PANDA queue via Harvester. 