# Multicluster Scheduler

This README describes the steps to set up a scheduling of jobs on multiple kubernetes clusters. After initial setup, the instuctions largely follow those on the [admiralty github page](https://github.com/admiraltyio/multicluster-scheduler). These instructions assume that a host cluster was already created with the kubespray setup documented in ../terraform_vm_cluster, but I'm not aware of any reason why they shouldn't work for any k8s clusters with unique cluster names. 

## Initial setup

### Build the second cluster with a unique cluster name
Assuming that the original host cluster was built with the default kubespray cluster name of cluster.local1, the cluster to be added **must be built with a different cluster name and user**, otherwise the kubectl context-switching won't work!! If building with the instructions in ../terraform_vm_cluster, just add an extra integer argument to `./create_k8s_cluster.sh install`, eg.

~~~
./create_k8s_cluster.sh install 2
~~~

### Copy the non-host cluster's kubeconfig to the host cluster

If needed, generate a public ssh key on the host cluster:

~~~
ssh-keygen
~~~

Go with the default options. 

Now, copy the contents of `~/.ssh/id_rsa.pub` on the host cluster into the `~/.ssh/authorized_keys` file on the non-host cluster to give the host cluster ssh access to the non-host cluster. From the host cluster:

~~~
scp [IP address of non-host cluster]:~/.kube/config ~/.kube/config2
~~~

### Update the `KUBECONFIG` variable to make both contexts available on the host machine

Set the `KUBECONFIG` variable as follows:

~~~
export KUBECONFIG=~/.kube/config:~/.kube/config2
~~~

Check that you can now see both contexts:

~~~
kubectl config get-contexts
~~~

should output:

~~~
CURRENT   NAME       CLUSTER          AUTHINFO           NAMESPACE
*         cluster1   cluster.local1   kubernetes-admin1   
          cluster2   cluster.local2   kubernetes-admin2
~~~

If the `CURRENT` context is set to cluster2 rather than cluster1, update the current context as follows:

~~~
kubectl config use-context cluster1
~~~

### Set up the Multi-cluster Scheduler

The following instructions are adapted from the README on the [multi-cluster scheduler github page](https://github.com/admiraltyio/multicluster-scheduler) to set up the multi-cluster scheduler on the original host machine, and test it out with the example nginx deployment.

## Getting Started

We assume that you are a cluster admin for two clusters, associated with, e.g., the contexts "cluster1" and "cluster2" in your kubeconfig. We're going to install a basic scheduler in cluster1 and agents in cluster1 and cluster2, but the scripts can easily be adapted for other configurations. Then, we will deploy a multi-cluster NGINX.

```bash
CLUSTER1=cluster1 # change me
CLUSTER2=cluster2 # change me
```


#### Scheduler

Choose a cluster to host the scheduler, download the basic scheduler's manifest and install it:

```bash
SCHEDULER_CLUSTER_NAME="$CLUSTER1"
RELEASE_URL=https://github.com/admiraltyio/multicluster-scheduler/releases/download/v0.3.0
kubectl config use-context "$SCHEDULER_CLUSTER_NAME"
kubectl apply -f "$RELEASE_URL/scheduler.yaml"
```

#### Federation

In the same cluster as the scheduler, create a namespace for the federation and, in it, a service account and role binding for each member cluster. The scheduler's cluster can be a member too.

```bash
FEDERATION_NAMESPACE=foo
kubectl create namespace "$FEDERATION_NAMESPACE"
MEMBER_CLUSTER_NAMES=("$CLUSTER1" "$CLUSTER2") # Add as many member clusters as you want.
for CLUSTER_NAME in "${MEMBER_CLUSTER_NAMES[@]}"; do
    kubectl create serviceaccount "$CLUSTER_NAME" \
        -n "$FEDERATION_NAMESPACE"
    kubectl create rolebinding "$CLUSTER_NAME" \
        -n "$FEDERATION_NAMESPACE" \
        --serviceaccount "$FEDERATION_NAMESPACE:$CLUSTER_NAME" \
        --clusterrole multicluster-scheduler-member
done
```

#### Multicluster-Service-Account

Install the custom multicluster-service-account manifest defined in the local install.yaml file in each member cluster:

```bash
for CLUSTER_NAME in "${MEMBER_CLUSTER_NAMES[@]}"; do
    kubectl --context "$CLUSTER_NAME" apply -f install.yaml
done
```

Then, download the kubemcsa binary and run the bootstrap command to allow member clusters to import service accounts from the scheduler's cluster:

```bash
MCSA_RELEASE_URL=https://github.com/admiraltyio/multicluster-service-account/releases/download/v0.3.1
OS=linux # or darwin (i.e., OS X) or windows
ARCH=amd64 # if you're on a different platform, you must know how to build from source
curl -Lo kubemcsa "$MCSA_RELEASE_URL/kubemcsa-$OS-$ARCH"
chmod +x kubemcsa
sudo mv kubemcsa /usr/local/bin

for CLUSTER_NAME in "${MEMBER_CLUSTER_NAMES[@]}"; do
    kubemcsa bootstrap "$CLUSTER_NAME" "$SCHEDULER_CLUSTER_NAME"
done
```

#### Agent

Use the custom agent manifest defined in agent.yaml and install it in each member cluster:

```bash
for CLUSTER_NAME in "${MEMBER_CLUSTER_NAMES[@]}"; do
    sed -e "s/SCHEDULER_CLUSTER_NAME/$SCHEDULER_CLUSTER_NAME/g" \
        -e "s/FEDERATION_NAMESPACE/$FEDERATION_NAMESPACE/g" \
        -e "s/CLUSTER_NAME/$CLUSTER_NAME/g" \
        agent.yaml > "agent-$CLUSTER_NAME.yaml"
    kubectl --context "$CLUSTER_NAME" apply -f "agent-$CLUSTER_NAME.yaml"
done
```

Check that node pool objects have been created in the agents' clusters and observations appear in the scheduler's cluster:

```bash
for CLUSTER_NAME in "${MEMBER_CLUSTER_NAMES[@]}"; do
    kubectl --context "$CLUSTER_NAME" get nodepools # or np
done
kubectl -n "$FEDERATION_NAMESPACE" get nodepoolobservations # or npobs
kubectl -n "$FEDERATION_NAMESPACE" get nodeobservations # or nodeobs
kubectl -n "$FEDERATION_NAMESPACE" get podobservations # or podobs
kubectl -n "$FEDERATION_NAMESPACE" get serviceobservations # or svcobs
# or, by category
kubectl -n "$FEDERATION_NAMESPACE" get observations --show-kind # or obs
```

### Example

Multicluster-scheduler's pod admission controller operates in namespaces labeled with `multicluster-scheduler=enabled`. In any of the member cluster, e.g., cluster2, label the `default` namespace. Then, deploy NGINX in it with the election annotation on the pod template:

```bash
kubectl --context "$CLUSTER2" label namespace default multicluster-scheduler=enabled
cat <<EOF | kubectl --context "$CLUSTER2" apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 10
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
      annotations:
        multicluster.admiralty.io/elect: ""
    spec:
      containers:
      - name: nginx
        image: nginx
        resources:
          requests:
            cpu: 100m
            memory: 32Mi
        ports:
        - containerPort: 80
EOF
```

Things to check:

1. The original pods have been transformed into proxy pods. Notice the replacement containers, and the original manifest saved as an annotation.
1. Proxy pod observations have been created in the scheduler's cluster. Notice their names indicating the proxy pods' cluster name, namespace and names. This information is also stored in `status.liveState.metadata`.
1. Delegate pod decisions have been created in the scheduler's cluster as well. Delegate pod decisions are named after the proxy pod observations, but the cluster names in `spec.template.metadata` don't necessarily match because they indicate the target clusters. Each decision was made based on all of the observations available at the time.
1. Delegate pods have been created in either cluster. Notice that their spec matches the original manifest.

```bash
kubectl --context "$CLUSTER2" get pods # (-o yaml for details)
kubectl -n "$FEDERATION_NAMESPACE" get podobs # (-o yaml)
kubectl -n "$FEDERATION_NAMESPACE" get poddecisions # or poddec (-o yaml)
for CLUSTER_NAME in "${MEMBER_CLUSTER_NAMES[@]}"; do
    kubectl --context "$CLUSTER_NAME" get pods # (-o yaml)
done
```

### Enforcing Placement

In some cases, you may want to specify the target cluster, rather than let the scheduler decide. For example, you may want an Argo workflow to execute certain steps in certain clusters, e.g., to be closer to external dependencies. You can enforce placement using the `multicluster.admiralty.io/clustername` annotation. [Admiralty's blog post](https://admiralty.io/blog/using-admiralty-s-multicluster-scheduler-to-run-argo-workflows-across-kubernetes-clusters) presents multicloud Argo workflows. To complete this getting started guide, let's annotate our NGINX deployment's pod template to reschedule all pods to cluster1.

```bash
kubectl --context "$CLUSTER2" patch deployment nginx -p '{
  "spec":{
    "template":{
      "metadata":{
        "annotations":{
          "multicluster.admiralty.io/clustername":"'$CLUSTER1'"
        }
      }
    }
  }
}'
```