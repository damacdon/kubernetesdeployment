# Kubefed v2 for cluster federation

This README documents the steps that worked for federating two kubespray clusters created on ComputeCanada using the terraform-kubespray setup documented in ../terraform_vm_cluster.

## 1. Obtain the kubefedctl command-line utility

Choose one of the clusters to be the host cluster that runs the KubeFed control plane. On this cluster, follow the instructions in the [KubeFed installation guide](https://github.com/kubernetes-sigs/kubefed/blob/master/docs/installation.md) to obtain the kubefedctl command-line utility. You can check for the latest version (v0.1.0-rc6 at the time of writing) on the [release page](https://github.com/kubernetes-sigs/kubefed/releases). 

~~~
VERSION=<latest-version, e.g. 0.1.0-rc6>
OS=<darwin/linux>
ARCH=amd64
curl -LO https://github.com/kubernetes-sigs/kubefed/releases/download/v${VERSION}/kubefedctl-${VERSION}-${OS}-${ARCH}.tgz
tar -zxvf kubefedctl-*.tgz
chmod u+x kubefedctl
sudo mv kubefedctl /usr/bin/ # make sure the location is in the PATH
~~~

Make sure you now have the kubefedctl command-line utility:

~~~
kubefedctl help
~~~

should output:

~~~
kubefedctl controls a Kubernetes Cluster Federation. Find more information at https://sigs.k8s.io/kubefed.

Usage:
  kubefedctl [flags]
  kubefedctl [command]

Available Commands:
  disable            Disables propagation of a Kubernetes API type
  enable             Enables propagation of a Kubernetes API type
  federate           Federate creates a federated resource from a kubernetes resource
  ...
~~~

## 2. Set up cluster contexts

### Update kubeconfig files

On the host cluster chosen above, open the .kube/config file, and change the cluster `name` under `-context` to `cluster1`. Also, change the `current-context` to `cluster1`. 

Open the .kube/config file on the other cluster and make the same changes, but replacing `cluster1` with `cluster2`. 


### Copy the non-host cluster's kubeconfig to the host cluster

If needed, generate a public ssh key on the host cluster:

~~~
ssh-keygen
~~~

Go with the default options. 

Now, copy the contents of `~/.ssh/id_rsa.pub` on the host cluster into the `~/.ssh/authorized_keys` file on the non-host cluster to give the host cluster ssh access to the non-host cluster. From the host cluster:

~~~
scp [IP address of non-host cluster]:~/.kube/config ~/.kube/config2
~~~

### Update the `KUBECONFIG` variable to make both contexts available on the host machine

Set the `KUBECONFIG` variable as follows:

~~~
export KUBECONFIG=~/.kube/config:~/.kube/config2
~~~

Check that you can now see both contexts:

~~~
kubectl config get-contexts
~~~

should output:

~~~
CURRENT   NAME       CLUSTER         AUTHINFO           NAMESPACE
*         cluster1   cluster.local   kubernetes-admin   
          cluster2   cluster.local   kubernetes-admin  
~~~

If the `CURRENT` context is set to cluster2 rather than cluster1, update the current context as follows:

~~~
kubectl config use-context cluster1
~~~

## 3. Helm Chart Deployment

This section follows the instructions in the [helm chart installation guide](https://github.com/kubernetes-sigs/kubefed/blob/master/charts/kubefed/README.md) to install the KubeFed control plane on the host cluster. 

### Download helm onto the host cluster

If needed, install wget:

~~~
sudo yum -y install wget
~~~

Now, get the latest helm binary and make it accessible at the command-line:

~~~
wget https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz
tar -zxvf helm-v2.14.3-linux-amd64.tar.gz 
mv linux-amd64/helm /usr/bin/helm
~~~

Make sure you now have the helm command-line utility available:

~~~
helm help
~~~

should output:

~~~
The Kubernetes package manager

To begin working with Helm, run the 'helm init' command:

	$ helm init

This will install Tiller to your running Kubernetes cluster.
It will also set up any necessary local configuration.

Common actions from this point include:

- helm search:    search for charts
- helm fetch:     download a chart to your local directory to view
...
~~~

### Configure RBAC for Helm 

Since RBAC is by default enabled on the kubespray clusters, the following is needed to ensure that helm is deployed with a service account with the permissions necessary to deploy KubeFed:

~~~
cat << EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF
~~~

Now, initialize helm with the tiller service account

~~~
helm init --service-account tiller
~~~

### Installing the Chart

First, add the KubeFed chart repo to your local repository.

~~~
helm repo add kubefed-charts https://raw.githubusercontent.com/kubernetes-sigs/kubefed/master/charts
~~~

~~~
helm repo list
~~~

should output:

~~~
NAME            URL
kubefed-charts   https://raw.githubusercontent.com/kubernetes-sigs/kubefed/master/charts
~~~

Check the available charts:

~~~
helm search kubefed
~~~

which should output something like

~~~
NAME                        	CHART VERSION	APP VERSION	DESCRIPTION                        
kubefed-charts/kubefed      	0.1.0-rc6    	           	KubeFed helm chart 
~~~

Install the kubefed chart, specifying the desired version:

~~~
helm install kubefed-charts/kubefed --name kubefed --version=<x.x.x> --namespace kube-federation-system
# Eg. helm install kubefed-charts/kubefed --name kubefed --version=0.1.0-rc6 --namespace kube-federation-system
~~~

## 4. Cluster Registration

This section follows the instructions in the [cluster-joining guide](https://github.com/kubernetes-sigs/kubefed/blob/master/docs/cluster-registration.md) to federate the clusters.

From the host cluster:

~~~
# Register the host cluster
kubefedctl join cluster1 --cluster-context cluster1 \
    --host-cluster-context cluster1 --v=2
    
# Add the non-host cluster to the cluster federation
kubefedctl join cluster2 --cluster-context cluster2 \
    --host-cluster-context cluster1 --v=2
~~~

Repeat this step to join any additional clusters.

Check the status of the joined clusters by using the following command:

~~~
kubectl -n kube-federation-system get kubefedclusters
~~~

which should output something like:

~~~
NAME       READY   AGE
cluster1   True    11h
cluster2   True    11h
~~~


